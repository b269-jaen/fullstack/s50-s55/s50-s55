import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import {Container} from 'react-bootstrap';
import Courses from './pages/Courses'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error'
import {useState, useEffect} from 'react'
import {UserProvider} from './UserContext'; 
import CourseView from './components/CourseView'

import './App.css';

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'

function App() {

  // state hook for the user state thats defined here is for global scope
  // to store the user information and will used for validation if user is logged in on the app or not
  // const [user, setUser] = useState({email: localStorage.getItem('email')})
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }



  // Used to check if user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      // User is logged in
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
      // User is logged out
      else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])

  return (
    // <></> - common pattern in React for component to return
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
        <Container>
          <AppNavbar />
          <Routes>  
            < Route path="/" element={<Home/>} />
            < Route path="/courses" element={<Courses/>}/>
            < Route path="/courses/:courseId" element={<CourseView/>}/>
            < Route path="/register" element={<Register/>}/>
            < Route path="/login" element={<Login/>}/>
            < Route path="/logout" element={<Logout/>}/>
            < Route path="*" element={<Error />}/>
          </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;
