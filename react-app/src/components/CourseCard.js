
// [S50 ACTIVITY]
import { Button, Row, Col, Card } from 'react-bootstrap';
import {useState} from 'react';
import {useEffect} from 'react';
import {Link} from 'react-router-dom';
// [S50 ACTIVITY END]


// [S50 ACTIVITY]
export default function CourseCard({course}) {

	// Deconstruct the course properties into their own variables
	const {name, description, price, _id} = course;

	/*
	SYNTAX:
		const [getter, setter] = useState(initialGetterValue);
	*/
	// const [count, setCount] = useState(0);
	// const [seats, setSeats] = useState(30);

	// // S51 activity
	// function enroll(){
	// 	/*if (count < 30){
	// 		setCount(count + 1)
	// 		setSeats(seats - 1)
	// 	}
	// 	else{
	// 		alert('no more seats.')
	// 	}*/

	// 	setCount(count + 1)
	// 	setSeats(seats - 1)

	// };

	// useEffect(() => {
	// 	if(seats <= 0){
	// 		alert("No more seats available!");
	// 	}
	// }, [seats]);


return (
    <Row className="mt-3 mb-3">
            <Col xs={12}>
                <Card className="cardHighlight p-0">
                    <Card.Body>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>PhP {price}</Card.Text>
                        {/*<Card.Subtitle>Enrollees</Card.Subtitle>
                        <Card.Text>{count} Enrollees</Card.Text>
                        <Card.Subtitle>Seats Available</Card.Subtitle>
                        <Card.Text>{seats} Seats</Card.Text>*/}
                        {/*<Button variant="primary" onClick={enroll} disabled={seats <= 0}>Enroll</Button>*/}
                        <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
                    </Card.Body>
                </Card>
            </Col>
    </Row>        
    )
}
// [S50 ACTIVITY END]





